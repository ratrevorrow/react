import React from 'react';
import { Grid, Segment, Container, Header, Dropdown } from 'semantic-ui-react';
import BSChart from './BSChart'; 

class BSItem extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            selection: ""
        }

        this.processSelection.bind(this);
        // this.filterInstrument.bind(this);

    }

    // filterInstrument(tradeData) {
    //     console.log(tradeData)
    //     // return this.props.tradeData.filter()
    // }

    processSelection() {

        const dropdown = [];

        this.props.allInstruments.forEach(element => {
            dropdown.push({
                key: element,
                text: element,
                value: element
            })
        });

        return (
            <Dropdown
                placeholder='Select Instrument'
                fluid
                selection
                // onChange={this.filterInstrument(this.props.tradeData)}
                options={dropdown}
            />
        )

    }

    render() {
        return (
            <Container style={{ paddingTop: '5rem' }}>
                <Grid stackable>
                    <Grid.Row>
                        <Grid.Column width={14}>
                            <Header as='h3' style={{ marginTop: '0.4rem' }}>
                                Market Transactions
                                {this.processSelection()}

                            </Header>
                            <Segment.Group size="small">
                                <Segment
                                    style={{
                                        paddingLeft: 0, paddingRight: 0,
                                        backgroundColor: '#f6f9fc'
                                    }}>
                                    <BSChart tradeData={this.props.tradeData} />
                                </Segment>
                            </Segment.Group>
                        </Grid.Column>

                    </Grid.Row>
                </Grid>
            </Container>
        )
    }
}

export default BSItem;