import React from 'react';
import InstruMenu from '../../menus/InstruMenu';
import BSItem from './BSItem';
import { APIGetDeals } from '../../../api';

export default class BS extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            tradeData: {},
            allInstruments: [],
        };
        this.getTradeData.bind(this);
        this.convertToChartData.bind(this);
    }

    componentDidMount() {
        this.getTradeData();
    }

    getTradeData() {
        APIGetDeals().then(
            res => {
                console.log(res.data);
                const data = this.convertToChartData(res.data);
                this.setState({ tradeData: res.data })

                const instrumentNames = []
                Object.keys(res.data).forEach(key => {
                    instrumentNames.push(res.data[key].instrumentName)
                })

                let removeDup = (instrumentNames) => instrumentNames.filter((v, i) => instrumentNames.indexOf(v) === i);
                const allInstruments = removeDup(instrumentNames);

                this.setState({ allInstruments: allInstruments })
            });
    }

    convertToChartData(resData) {
        let i = 0;
        const instrumentNames = []
        const data = {
            labels: ['8am', '9am', '10am', '11am', '12nn', '8am', '9am', '10am', '11am', '12nn', '8am', '9am', '10am', '11am', '12nn'],
            datasets: []
        }

        Object.keys(resData).forEach(key => {
            instrumentNames.push(resData[key].instrumentName)
        })

        let removeDup = (instrumentNames) => instrumentNames.filter((v, i) => instrumentNames.indexOf(v) === i);
        const allInstruments = removeDup(instrumentNames);


        allInstruments.forEach(elem => {
            const dataSet = {
                label: elem,
                fill: false,
                lineTension: 0.1,
                backgroundColor: this.randomRGBA(i),
                borderColor: this.randomRGBA(i),
                borderCapStyle: 'butt',
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: 'miter',
                pointBorderColor: this.randomRGBA(i),
                pointBackgroundColor: '#fff',
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: this.randomRGBA(i),
                pointHoverBorderColor: this.randomRGBA(i),
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: []
            }
            data.datasets.push(dataSet);
            ++i;
        })

        Object.keys(resData).forEach(key => {
            data.datasets.forEach(elem => {
                if (elem.label == resData[key].instrumentName) {
                // if (elem.label == "Heliosphere") {
                    console.log(resData[key].price)
                    if (elem.data.length < 15) {
                        elem.data.push(resData[key].price);
                    } else {
                        elem.data.pop();
                        elem.data.push(resData[key].price);
                    }
                }
            })
        })
        console.log(data);
        return data;
    }

    randomRGBA(i) {
        const rgbaArray = [
            'rgba(0,0,0,1)',
            'rgba(0,0,128,1)',
            'rgba(0,128,0,1)',
            'rgba(128,0,128,1)',
            'rgba(128,128,0,1)',
            'rgba(255,255,0,1)',
            'rgba(255,0,0,1)',
            'rgba(0,255,0,1)',
            'rgba(0,180,0,1)',
            'rgba(99,57,34,1)',
            'rgba(24,24,146,1)',
            'rgba(0,57,94,1)',
            'rgba(24,57,24,1)'
        ]
        return rgbaArray[i];
    }


    render() {
        return (
            <div>
                <InstruMenu />
                <BSItem allInstruments={this.state.allInstruments} tradeData={this.convertToChartData(this.state.tradeData)} />
            </div>
        )
    }

}