import React from 'react';
import InstruMenu from '../../menus/InstruMenu';
import HistoryItem from './HistoryItem';
import { Grid, Segment, Container, Header } from 'semantic-ui-react';
import { APIGetDeals } from '../../../api';





class History extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            tradeHistory: {}
        };
        this.getTradeHistory.bind(this);
    }

    componentDidMount() {
        this.getTradeHistory();
    }

    getTradeHistory() {
        APIGetDeals().then(
            res => {
                console.log(res.data);
                this.setState({ tradeHistory: res.data })
            });
    }





    render() {
        return (
            <div>
                <InstruMenu/>
                <Container style={{ paddingTop: '5rem' }}>
                <Grid stackable>
                    <Grid.Row>
                        <Grid.Column width={16}>
                            <Header as='h3' style={{ marginTop: '0.4rem' }}>
                                Trade History
                            </Header>
                            <Segment.Group size="small">
                                <HistoryItem tradeHistory={this.state.tradeHistory}/>
                            </Segment.Group>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Container>
            </div>
        )
    }
}

export default History;