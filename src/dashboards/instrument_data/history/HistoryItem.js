import React from 'react';
import { Table } from 'semantic-ui-react';




class HistoryItem extends React.Component {

    constructor(props) {
        super(props);

        this.generateHistoryItems.bind(this);
    }

    generateHistoryItems() {

        console.log(this.props.tradeHistory)

        const tradeArr = [];
        Object.keys(this.props.tradeHistory).forEach(key => {
            tradeArr.push(this.props.tradeHistory[key]);
        })

        return (
            <div>
                <Table celled>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>Instrument</Table.HeaderCell>
                            <Table.HeaderCell>Price</Table.HeaderCell>
                            <Table.HeaderCell>Time</Table.HeaderCell>
                            <Table.HeaderCell>Type</Table.HeaderCell>
                            <Table.HeaderCell>Counter Party</Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>

                    <Table.Body>
                          {/* className="positionListItem" */}
                            {
                                 tradeArr.map(history => (
                                    <Table.Row>
                                        <Table.Cell>{history.instrumentName}</Table.Cell>
                                        <Table.Cell>{history.price}</Table.Cell>
                                        <Table.Cell>{history.quantity}</Table.Cell>
                                        <Table.Cell>{history.time}</Table.Cell>
                                        <Table.Cell>{history.type}</Table.Cell>
                                        <Table.Cell>{history.cpty}</Table.Cell>
                                    </Table.Row>
                                ))
                            }
                    </Table.Body>
                </Table>
            </div>

        )
    }



    render() {
        return (
            this.generateHistoryItems()
        );
    }
}

export default HistoryItem;