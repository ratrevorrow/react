import React from 'react';
import { Grid, Segment, Container, Header } from 'semantic-ui-react';
import PositionItem from './PositionItem';
import EffectivePL from './EffectivePL';
import RealisedPL from './RealisedPL';

class Positions extends React.Component {

    constructor(props) {
        super(props);
        this.generatePositions.bind(this);
    }

    generatePositions() {
        let rp = 0;
        let ep = 0;
        Object.keys(this.props.instruments).forEach(key => {
            ep += this.props.instruments[key].effective;
            rp += this.props.instruments[key].realised;

            console.log(key);
            console.log(this.props.instruments[key].effective);
        })


        return (
            <div>
                <Container style={{ paddingTop: '5rem' }}>
                    <Grid stackable>
                        <Grid.Row>
                            <Grid.Column width={8}>
                                <Header as='h3' style={{ marginTop: '0.4rem' }}>
                                    Positions
                            </Header>
                                <Segment.Group size="small">
                                    <Segment
                                        style={{
                                            paddingLeft: 0, paddingRight: 0,
                                            backgroundColor: '#f6f9fc'
                                        }}>
                                        <PositionItem instruments={this.props.instruments} />
                                    </Segment>
                                </Segment.Group>
                            </Grid.Column>
                            <Grid.Column width={4}>
                                <Header as='h3' style={{ marginTop: '0.4rem' }}>
                                    Realised Profit
                            </Header>
                                <Segment.Group size="small">
                                    <Segment
                                        style={{
                                            paddingLeft: 0, paddingRight: 0,
                                            backgroundColor: '#f6f9fc'
                                        }}>
                                        <RealisedPL rpl={rp} />
                                    </Segment>
                                </Segment.Group>
                            </Grid.Column>
                            <Grid.Column width={4}>
                                <Header as='h3' style={{ marginTop: '0.4rem' }}>
                                    Effective Profit
                            </Header>
                                <Segment.Group size="small">
                                    <Segment
                                        style={{
                                            paddingLeft: 0, paddingRight: 0,
                                            backgroundColor: '#f6f9fc'
                                        }}>
                                        <EffectivePL epl={ep} />
                                    </Segment>
                                </Segment.Group>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </Container>
            </div>
        )

    }


    render() {
        return (
            this.generatePositions()
        );
    }
}

export default Positions;