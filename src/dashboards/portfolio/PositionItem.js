import React from 'react';
import { Grid, Table } from 'semantic-ui-react';

class PositionItem extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
        }

        this.generateListItem.bind(this);
    }

    // componentDidMount() {
    //     this.extractInstrumentNames();
    // }

    generateListItem() {
        const instrumentArr = [];
        Object.keys(this.props.instruments).forEach(key => {
            instrumentArr.push(this.props.instruments[key]);
        })

        return (
            <div>
                <Table celled>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>Instrument</Table.HeaderCell>
                            <Table.HeaderCell>Quantity</Table.HeaderCell>
                            <Table.HeaderCell>Weighted Price</Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>

                    <Table.Body>
                          {/* className="positionListItem" */}
                            {
                                instrumentArr.map(instrument => (
                                <Table.Row>
                                    <Table.Cell>{instrument.inst}</Table.Cell>
                                    <Table.Cell>
                                        <span className="name">{instrument.total_number_of_shares}</span>
                                    </Table.Cell>
                                    <Table.Cell width={1} >
                                         <span className="name">{instrument.weighted}</span>
                                    </Table.Cell>
                                    
                                </Table.Row>
                                ))}
                        
                    </Table.Body>
                </Table>
            </div>
        )
    }

    render() {
        return (
            this.generateListItem()
        );
    }
}

export default PositionItem;