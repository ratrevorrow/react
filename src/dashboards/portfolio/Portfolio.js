import React from 'react';
import PIMenu from '../menus/PIMenu';
import Positions from './Positions';
import { APIGetPortfolio } from '../../api';

export default class Portfolio extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            instruments: {},
        };

        this.getPortfolioItem.bind(this);
    }

    componentDidMount() {
        this.getPortfolioItem();
    }



    getPortfolioItem() {
        APIGetPortfolio().then(
            res => {
                console.log(res.data);
                this.setState({ instruments: res.data })
            });
    }


    render() {
        return (
            <div>
                <PIMenu />
                <Positions instruments={this.state.instruments} />
            </div>
        )
    }

}