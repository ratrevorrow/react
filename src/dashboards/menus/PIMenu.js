import React, { Component } from 'react'
import { Menu, Button } from 'semantic-ui-react'
import { Link } from 'react-router-dom'

export default class PIMenu extends Component {

    constructor(props) {
        super(props);
    }
    
    state = {}

    handleItemClick = (e, { name }) => this.setState({ activeItem: name })

    render() {
        const { activeItem } = this.state

        return (
            <Menu>
                <Menu.Item
                    name='Portfolio'
                    active={activeItem === 'Portfolio'}
                    onClick={this.handleItemClick}
                    as={ Link }
                    to='portfolio'
                >
                    Portfolio
                </Menu.Item>

                <Menu.Item 
                    name='Instrument Data' 
                    active={activeItem === 'Instrument Data'} 
                    onClick={this.handleItemClick}
                    as={ Link }
                    to='buysell'>
                    Instrument Data
                </Menu.Item>
                 
                <Link to="/"><Button>Logout</Button></Link>
            </Menu>
        )
    }
}