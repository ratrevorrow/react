import React, { Component } from 'react'
import { Menu, Button } from 'semantic-ui-react'
import { Link } from 'react-router-dom'

export default class PIMenu extends Component {

    constructor(props) {
        super(props);
    }
    
    state = {}

    handleItemClick = (e, { name }) => this.setState({ activeItem: name })

    render() {
        const { activeItem } = this.state

        return (
            <Menu>
                <Menu.Item
                    name='Buy/Sell'
                    active={activeItem === 'Buy/Sell'}
                    onClick={this.handleItemClick}
                    as={Link}
                    to='buysell'
                >
                    Buy/Sell
                </Menu.Item>

                <Menu.Item 
                    name='Trade History' 
                    active={activeItem === 'Trade History'} 
                    onClick={this.handleItemClick}
                    as={ Link }
                    to='history'
                    >
                    Trade History
                </Menu.Item>

                <Menu.Item 
                    name='Back To Portfolio' 
                    active={activeItem === 'Back To Portfolio'} 
                    onClick={this.handleItemClick}
                    as={ Link }
                    to='portfolio'
                    >
                        Back To Porfolio
                </Menu.Item>
                
                
                <Link to="/"><Button>Logout</Button></Link>
                

            </Menu>
        )
    }
}