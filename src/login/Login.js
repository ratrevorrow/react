import React from 'react'
import { Button, Form, Grid, Header, Segment } from 'semantic-ui-react'
import { APIsubmitForm } from '../api.js'

class LoginForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            message: ''
        };

        this.handleChangeUser = this.handleChangeUser.bind(this);
        this.handleChangePassword = this.handleChangePassword.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChangeUser(event) {
        this.setState({ username: event.target.value });
    }

    handleChangePassword(event) {
        this.setState({ password: event.target.value });
    }

    handleSubmit() {
        const json = {
            username: this.state.username,
            password: this.state.password
        }


        APIsubmitForm(json)
            .then(res => {
                // console.log(typeof( res.data.message ));
                // console.log( res.data.message );
                const type_user = res.data.type_user;
                if (res.data.message === 'success') {
                    this.setState({ message: "Successful login" });
                    if (type_user === 'trader') this.props.history.push("/portfolio");
                    else if (type_user === 'seniortrader') this.props.history.push("/senior_info");
                    else if (type_user === 'security') this.props.history.push("/security");
                    else if (type_user === 'sysarch') this.props.history.push("/portfolio");
                } else {
                    this.setState({ message: "Incorrect username / password" });
                }

                
            });
        // .then(this.props.history.push("/portfolio"));

        ;
    }

    render() {
        return (
            <Grid textAlign='center' style={{ height: '100vh' }} verticalAlign='middle'>
                <Grid.Column style={{ maxWidth: 450 }}>
                    <Header as='h2' color='teal' textAlign='center'>
                        Log in to your account</Header>
                    <Form size='large' onSubmit={this.handleSubmit}>
                        <Segment stacked>
                            <Form.Input
                                fluid icon='user'
                                iconPosition='left'
                                placeholder='username'
                                onChange={this.handleChangeUser}
                                required />
                            <Form.Input
                                fluid
                                icon='lock'
                                iconPosition='left'
                                placeholder='Password'
                                type='password'
                                onChange={this.handleChangePassword}
                                required
                            />
                            <Button content='Login' primary />
                        </Segment>
                    </Form>
                    <p>{this.state.message}</p>
                </Grid.Column>
            </Grid>
        );
    }
}



export default LoginForm;