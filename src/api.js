import axios from 'axios';

const api = axios.create({
    baseURL: 'http://business-logic-server-2-react-master.apps.dbgrads-6eec.openshiftworkshop.com/',
    withCredentials: true,
});

api.defaults.headers.post['Content-Type'] = 'application/json';

export async function APIsubmitForm(formData) {
    return api.request({
        url: "/submitForm",
        method: 'post',
        data: JSON.stringify(formData)
    })
}

export async function APIsync() {
    return api.request({
        url: "/sync",
        method: 'get'
    })
}

export async function APIGetPortfolio() {
    return api.request({
        url: "/portfolio",
        method: 'get'
    })
}

export async function APIGetDeals() {
    return api.request({
        url: "/deals",
        method: 'get'
    })
}