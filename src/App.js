import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import LoginForm from './login/Login.js';
import DashMenu from './dashboards/menus/PIMenu.js';
import Portfolio from './dashboards/portfolio/Portfolio.js';
import BS from './dashboards/instrument_data/buysell/BS.js';
import History from './dashboards/instrument_data/history/History.js';
import Sync from './security-officer/SecurityOfficer';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Router>
          <Route exact path="/" component={LoginForm} />
          <Route path="/dashmenu" component={DashMenu} />
          <Route path="/portfolio" component={Portfolio} />
          <Route path="/buysell" component={BS} />
          <Route path="/history" component={History} />
          <Route path="/security" component={Sync} />
        </Router>
      </header>
    </div>
  ); 
}


export default App;
