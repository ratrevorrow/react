import React from 'react'
import { Menu, Button, Form, Grid, Header, Segment } from 'semantic-ui-react'
import { APIsync } from '../api.js';
import { Link } from 'react-router-dom';


class Sync extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            message: "Connecting to database"
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.syncDB();
    }

    handleSubmit() {
        this.setState({ message: "Connecting to database" });
        this.syncDB();
    }

    syncDB(){
        APIsync()
            .then(event => {
                console.log(event);
                this.setState({ message: event.data.message })
        });
    }

    render() {
        return (
            <div>
                <Menu>
                    <Link to="/"><Button>Logout</Button></Link>
                </Menu>
                <Grid textAlign='center' style={{ height: '100vh' }} verticalAlign='middle'>
                    <Grid.Column style={{ maxWidth: 450 }}>
                        <Header as='h2' color='teal' textAlign='center'>
                            Welcome Security Officer</Header>
                        <Form size='large' onSubmit={this.handleSubmit}>
                            <Segment stacked>
                                <Button content='Resync' primary />
                            </Segment>
                        </Form>
                        <p> {this.state.message} </p>
                    </Grid.Column>
                </Grid>
            </div>
        );
    }
}

export default Sync;